package Utilities_I19ab;


import java.net.InetAddress;
import java.net.UnknownHostException;


class GetServerName{

	
public static String ServerName(){	
	
String hostname = "Unknown";

try
{
    InetAddress addr;
    addr = InetAddress.getLocalHost();
    hostname = addr.getHostName();
}
catch (UnknownHostException ex)
{
    System.out.println("Hostname can not be resolved");
}


finally{
	
	return hostname;

}

}


public static void main(String args[]){


System.out.println(GetServerName.ServerName());


	

}




}