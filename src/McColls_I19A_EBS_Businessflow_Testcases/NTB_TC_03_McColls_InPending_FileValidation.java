package McColls_I19A_EBS_Businessflow_Testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Functions.WinSCPCall;
import McColls_EBS_I19AB_Functions.*;
import Utilities_I19All.*;
import Utilities_I19ab.MyException;
import Utilities_I19ab.Reporting_Utilities;
import Utilities_I19ab.RowGenerator;

import com.google.common.base.Functions.*;

public class NTB_TC_03_McColls_InPending_FileValidation {
	
	
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	
	String TestDataPath;
	String TemporaryFilePath;
	
	String ResultPath="";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		
		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("McColls_I19_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I19_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I19_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I19_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I19_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I19_SheetName");
		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("McColls_I19_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I19_TemporaryFilePath");
		
		System.out.println(BrowserPath);
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		
		utilityFileWriteOP.writeToLog("*********************************END**********************************");	
		
		
	}


	@Test
	public void test() throws IOException {
		
		Boolean Res_ExtractInvoiceID=false;
		Boolean Res_InvoiceValidationEBS=false;
		String EBSInvoiceID=null;
		

		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "McCollsI19_Verify_Invoices_NonTobacco";
		//String DriverSheetPath = "Test Data/DriverExcel.xls";
		
		String DriverSheetPath = TestDataPath;
		
				//String DriverSheetPath = "C:/LocalBatchTestRunner/Test Data/DriverExcel.xls";
		
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		
		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;
		
		//--------------------Doc File---------------------
				XWPFDocument doc = new XWPFDocument();
		        XWPFParagraph p = doc.createParagraph();
		        XWPFRun xwpfRun = p.createRun();
		        
		        String consolidatedScreenshotpath="";
		        //-------------------------------------------------
				
		
		int r = 0;
		try {
			
			int rows = 0;
			int cols = 0;
			int occurances = 0;
			
			Workbook wrk1 = Workbook.getWorkbook(new File(DriverSheetPath));
			
			Sheet sheet1 = wrk1.getSheet("McColls I19");
			
			rows = sheet1.getRows();
			cols = sheet1.getColumns();
			
			for(r=1; r<rows; r++) {
				
				TestCaseNo = sheet1.getCell(0, r).getContents().trim();
				TestCaseName = sheet1.getCell(1, r).getContents().trim();
				Keyword = sheet1.getCell(2, r).getContents().trim();
				
				ProxyHostName = sheet1.getCell(5, r).getContents().trim();
				ProxyPort = sheet1.getCell(6, r).getContents().trim();
				SYSUserName = sheet1.getCell(7, r).getContents().trim();
				SYSPassWord = sheet1.getCell(8, r).getContents().trim();
				
				TargetHostName = sheet1.getCell(9, r).getContents().trim();
				TargetPort = sheet1.getCell(10, r).getContents().trim();
				TargetHeader = sheet1.getCell(11, r).getContents().trim();
				UrlTail = sheet1.getCell(12, r).getContents().trim();
				ApiKey = sheet1.getCell(13, r).getContents().trim();
				AuthorizationKey = sheet1.getCell(14, r).getContents().trim();
				AuthorizationValue =sheet1.getCell(15, r).getContents().trim();
				
				AllOrderIDs = sheet1.getCell(16, r).getContents().trim();
				AllMessageTypes = sheet1.getCell(17, r).getContents().trim();
				AllShipToLocationIds = sheet1.getCell(18, r).getContents().trim();
				ShipToDeliverAt = sheet1.getCell(19, r).getContents().trim();
				

				SFTPHostName = sheet1.getCell(26, r).getContents().trim();
				SFTPPort = sheet1.getCell(27, r).getContents().trim();
				SFTPUserName = sheet1.getCell(28, r).getContents().trim();
				SFTPPassword = sheet1.getCell(29, r).getContents().trim();
				NasPath = sheet1.getCell(30, r).getContents().trim();
				
				
				Final_Result=sheet1.getCell(3, r).getContents().trim();

			
				
				Final_Result=sheet1.getCell(3, r).getContents().trim();

		
				
		if(occurances>0){
			          
			    	if(!(TestKeyword.contentEquals(Keyword))){

				    	break;
				    	
			  }

		 }
				
 if(Keyword.equalsIgnoreCase(TestKeyword)) {
					

					System.out.println("Executing....");
					  
					 occurances=occurances+1;
				
					 String OrderID[] = AllOrderIDs.split(",");
					
					 
				
	
			 
//-------------------HTML Header--------------------
			 
Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
				
//----------------------------------------------------

String latFilename =WinSCPCall.latestFileExtract_SFTP(AllOrderIDs,SFTPUserName, SFTPPassword,NasPath, SFTPHostName,SFTPPort,TestCaseNo, ResultPath, xwpfRun);

if(latFilename !=null) {
	
	 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Shipment File at Nas Path validated Successfully.", "PASS", ResultPath);
	}
		
else{
	
	Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Validate Shipment File at Nas Path", "Error Occured during Validation of Shipment File at Nas Path.", "FAIL", ResultPath);
	
	throw new MyException("Test Stopped Because of Failure. Please check Execution log");
}

		
		
		
		if((latFilename!=null)){
		
			Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Validate Shipment File at Nas Path", "Shipment file is successfully generated.", "PASS", ResultPath);

			 
			Final_Result="PASS"; 

			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, "McColls I19");
				  
			Assert.assertTrue( TestCaseNo+"--"+TestCaseName,true);
		
		}
		

		else{
			Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Validate Shipment File at Nas Path", "Error Occured at NAs path file validation.", "FAIL", ResultPath);

			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, "McColls I19");
						 
			Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}
		
	
					 
		}
	}
			
		} 
		
		
		catch (Exception e) {
		
			Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Error while connection at SFTP", "Error Occured while connection at SFTP", "FAIL", ResultPath);
			System.out.println(e);
			Final_Result="FAIL"; 
			   
		     excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, "Standard Amazon");
					  
		     Assert.assertTrue(TestCaseNo+"--"+TestCaseName,false);
		}
		finally
		 {   
			//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
		        RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		 }
			
		}
}

