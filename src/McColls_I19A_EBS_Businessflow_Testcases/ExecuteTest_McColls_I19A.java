package McColls_I19A_EBS_Businessflow_Testcases;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	
	
	McColls_I19A_EBS_Businessflow_Testcases.OrderIDCreation_run.class,
	
	McColls_I19A_EBS_Businessflow_Testcases.EBS_NTB_TC_02_McColls_OrderCreation_NonTobacco.class,
	
	McColls_I19A_EBS_Businessflow_Testcases.NTB_TC_03_McColls_InPending_FileValidation.class,
	
	McColls_I19A_EBS_Businessflow_Testcases.Execute_XXWMM_WHOLESALE_IMPORT.class,
	
	McColls_I19A_EBS_Businessflow_Testcases.NTB_TC_05_McColls_OutPending_FileValidation.class,
	
	McColls_I19A_EBS_Businessflow_Testcases.EBS_NTB_TC_02_McColls_InvoiceValidation_NonTobacco.class,
	
})

public class ExecuteTest_McColls_I19A {
	
	
	

 
} 