package McColls_I19A_EBS_Businessflow_Testcases;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import Utilities_v1.*;

public class BatchJoB_XXWMM_WHOLESALE_IMPORT {
	
	
	
	public static void main(String args[]){
	
	String JobName="GET_WHOLESALE_ORDERS_SERVICE";
	
	String TestCaseNo="TC_01";
	String UserName="SYSTCS19";
	String Password="Changeme";
	
	
	
	String DataFilePath="S:\\MTEBS\\Automation\\McColls_I23_Selenium\\McCollsI23\\TestData\\Appworx_Login_Local.xls";
	
	
	String ResultPath="S:\\MTEBS\\Automation\\McColls_I23_Selenium\\McCollsI23\\Results";
	
	String AppworxURL="http://teapx02x.unix.morrisons.net:5050/APXSITBA/Intro.html";
	
	BatchJoB_XXWMM_WHOLESALE_IMPORT.RunJob(JobName, TestCaseNo,ResultPath,UserName,Password,AppworxURL);


	}
	
	


public static boolean  RunJob (String link,String userName,String userPwd,String TestCaseNo,String ResultPath,String JobName) {
	
	Boolean  result=false;
	 try {

	       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
		
	      Process p=  Runtime.getRuntime().exec( "cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"+" "+link+","+userName+","+userPwd+","+TestCaseNo+","+ResultPath+","+JobName);
	      
	      System.out.println("Waiting for UFT Script Execution to be completed ...");
	        try {
				p.waitFor();
				
				result=true;
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
//	        String batchstatus=utilityFileWriteOP.ReadBatchResult(batchStatusFolder);
	        
	        
}
	 
	 
	 catch (Exception ex) {
		 
		 System.out.println("Problem occurred during batch run...");
		 result=false;

	    }
	 
	 
	 finally{
    	   
    	return result;   
    	   
       }
	
	
}


	
	public static boolean  RunJob_old (String JobName,String TestCaseNo,String ResultPath,String UserName,String Password,String AppworxURL) {
		
		Boolean  result=false;
		 try {

		       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
			
		      Process p=  Runtime.getRuntime().exec( "cscript C:/Appworx_Job_Run/Appworx_BatchParam_WS1.vbs"+" "+TestCaseNo+","+JobName+","+ ResultPath+","+UserName+","+Password+","+AppworxURL);
		      
		      System.out.println("Waiting for UFT Script Execution to be completed ...");
		        try {
					p.waitFor();
					
					result=true;
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        
		       
		        
		    }
		 
		 
		 catch (IOException ex) {

		    }
		 
		 
		 finally{
	    	   
	    	return result;   
	    	   
	       }
		
		
	}

}
