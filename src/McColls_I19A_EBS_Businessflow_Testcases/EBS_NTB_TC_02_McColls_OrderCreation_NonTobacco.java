package McColls_I19A_EBS_Businessflow_Testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import McColls_EBS_I19AB_Functions.*;
import McColls_I5a_All_Functions.McColls_I5a_GetCall;
import McColls_I5a_All_Functions.ShipCall;
import Utilities_I19All.*;
import Utilities_I19ab.MyException;
import Utilities_I19ab.Reporting_Utilities;
import Utilities_I19ab.RowGenerator;
import Utilities_I19ab.excelCellValueWrite;

public class EBS_NTB_TC_02_McColls_OrderCreation_NonTobacco {
	
	//String URL;
			String DriverPath;
			String DriverName;
			String DriverType;
			String BrowserPath;
			String ServerName;
			String SheetName;
			String ItemDetailsSheetName;
			
			String TestDataPath;
			String TemporaryFilePath;
			
			String ResultPath="";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		
		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("McColls_I19_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I19_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I19_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I19_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I19_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I19_SheetName");
		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("McColls_I19_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I19_TemporaryFilePath");
		
		System.out.println(BrowserPath);
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		utilityFileWriteOP.writeToLog("*********************************End**********************************");
		
	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = null;
		String TestKeyword = "McCollsI19_Verify_Invoices_NonTobacco";
		
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		String OrderCount=null;
		
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;
		
		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;
		
		String ColumnNames = null;
		String ItemIDs = null;
		String QuantitytoShip = null;

		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        
        String consolidatedScreenshotpath="";
        //-------------------------------------------------
		
		int r = 0;
		try{
			
		
		
		int rows = 0;
		//int cols = 0;
		int occurances = 0;
		int ItemDetailsoccurances = 0;
		
		Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	
		Sheet sheet1 = wrk1.getSheet(SheetName);
		Sheet sheet2 = wrk1.getSheet(ItemDetailsSheetName);
		
		rows = sheet1.getRows();
		//cols = sheet1.getColumns();
		int itemdetailsrows = sheet2.getRows();
		System.out.println(itemdetailsrows);
		
		for(int i=1; i<itemdetailsrows; i++) {
			
			Keyword = sheet2.getCell(0, i).getContents().trim();
			
			System.out.println(sheet2.getCell(0, i).getContents().trim());
			if(ItemDetailsoccurances>0){
		          
		    	if(!(TestKeyword.contentEquals(Keyword))){

			    	break;
			    	
		    	}

			 }
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				ItemDetailsoccurances=ItemDetailsoccurances+1;
				ItemIDs = sheet2.getCell(1, i).getContents().trim();
				
					System.out.println("Matched "+Keyword + sheet2.getCell(0, i).getContents().trim());

			}
		}
		
		for(r=1; r<rows; r++) {
			
			TestCaseNo = sheet1.getCell(0, r).getContents().trim();
			TestCaseName = sheet1.getCell(1, r).getContents().trim();
			Keyword = sheet1.getCell(2, r).getContents().trim();
			
			ProxyHostName = sheet1.getCell(5, r).getContents().trim();
			ProxyPort = sheet1.getCell(6, r).getContents().trim();
			SYSUserName = sheet1.getCell(7, r).getContents().trim();
			SYSPassWord = sheet1.getCell(8, r).getContents().trim();
			
			TargetHostName = sheet1.getCell(9, r).getContents().trim();
			TargetPort = sheet1.getCell(10, r).getContents().trim();
			TargetHeader = sheet1.getCell(11, r).getContents().trim();
			UrlTail = sheet1.getCell(12, r).getContents().trim();
			ApiKey = sheet1.getCell(13, r).getContents().trim();
			AuthorizationKey = sheet1.getCell(14, r).getContents().trim();
			AuthorizationValue =sheet1.getCell(15, r).getContents().trim();
						
			AllOrderIDs = sheet1.getCell(16, r).getContents().trim();
			AllMessageTypes = sheet1.getCell(17, r).getContents().trim();
			AllShipToLocationIds = sheet1.getCell(18, r).getContents().trim();
			ShipToDeliverAt = sheet1.getCell(19, r).getContents().trim();
			
			AllOrderAttributes = sheet1.getCell(21, r).getContents().trim();
			AllMessageAttributes = sheet1.getCell(22, r).getContents().trim();
			AllShipToAddressAttributes = sheet1.getCell(23, r).getContents().trim();
			AllBillToAddressAttributes = sheet1.getCell(24, r).getContents().trim();
			AllBillToTaxAddressAttributes = sheet1.getCell(25, r).getContents().trim();
			
			SFTPHostName = sheet1.getCell(26, r).getContents().trim();
			SFTPPort = sheet1.getCell(27, r).getContents().trim();
			SFTPUserName = sheet1.getCell(28, r).getContents().trim();
			SFTPPassword = sheet1.getCell(29, r).getContents().trim();
			NasPath = sheet1.getCell(30, r).getContents().trim();
			
			ColumnNames = sheet1.getCell(31, r).getContents().trim();
			QuantitytoShip = sheet1.getCell(33, r).getContents().trim();
			
			//String quantityType = sheet1.getCell(49, r).getContents().trim();
			String quantityOrdered = sheet1.getCell(49, r).getContents().trim();
			OrderCount = sheet1.getCell(51, r).getContents().trim();
			
			
			if(occurances>0){
		        
		    	if(!(TestKeyword.contentEquals(Keyword))){

			    	break;
			    	
		    	}

			 }
			
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				 occurances=occurances+1;
				 
				 String OrderID[] = AllOrderIDs.split(",");
				 String ShipToLocationId[] = AllShipToLocationIds.split(",");
				 String MessageType[] = AllMessageTypes.split(",");
				 String AllColumnNames[] = ColumnNames.split(",");
				 //System.out.println(ItemIDs);
				 String AllItemIDs[] = ItemIDs.split(",");
				 //String ItemID[] = AllItemIDs.split(",");
				 String AllQuantityShipped[] = QuantitytoShip.split(",");

				 boolean post = true;
				
				 boolean get = true;
				 
				 
				 
				 boolean ship = true;
					
				 boolean aftership = true;
				 
				 Boolean RestShip=false;
				 
				 boolean RestShipppedClosure= false;
				 
				 Boolean RestGetAfterShip=false;
				 
				 Boolean RestGetItem=true;
				 
				 Boolean RestGet=true;

				 
				 int order = 0;
				 
				 String[] AllItemIds = ItemIDs.split(",");
				 String[] AllQuantitiesOrdered = quantityOrdered.split(",");
				 //String[] AllQuanityTypes = quantityType.split(",");
				 
				
				 
				 
				 System.out.println("Ship to location Id length is "+ShipToLocationId.length);
				 
				 
				 for(int k=0; k<AllItemIds.length; k++) {
					 
					 String quantityType = McColls_I19_GetCall.GetCallquantityTypeFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIDs[k], TestCaseNo, ResultPath);

					 
					 String RestGetItemDetails = McColls_I19_GetCall.GetCallItemDetailsFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[k], "skuPin", quantityType, String.valueOf(k+1), AllQuantitiesOrdered[k], TestCaseNo, ResultPath);
					 
					 
					 int itemDetailsPrintOccurence = 0;
					 
					 for(int i=1; i<itemdetailsrows; i++) {
							
							Keyword = sheet2.getCell(0, i).getContents().trim();
							
							System.out.println(sheet2.getCell(0, i).getContents().trim());
							if(itemDetailsPrintOccurence>0){
						          
						    	if(!(TestKeyword.contentEquals(Keyword))){
						    		System.out.println("breaking with keyword "+ Keyword);
							    	break;
							    	
						    	}

							 }
							if(Keyword.equalsIgnoreCase(TestKeyword)) {
								  
								itemDetailsPrintOccurence=itemDetailsPrintOccurence+1;
								System.out.println("About To write the big thing"); 
								 excelCellValueWrite.writeValueToCell(RestGetItemDetails, i, 2+k, TestDataPath, ItemDetailsSheetName);

	
							}
						}
					 
					 
				 }
				 
					File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					
					
					consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_"; 
					
				 					
				 
//-------------------HTML Header--------------------
				 
Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					
//----------------------------------------------------
			 

for(int vatAmountindex=0; vatAmountindex<AllItemIds.length; vatAmountindex++) {
	
	String VatAmount = McColls_I19_GetCall.GetCallVatAmountFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, AllItemIds[vatAmountindex], TestCaseNo, ResultPath);

	Utilities_I19All.utilityFileWriteOP.appendValueToCell(VatAmount, r, 46, TestDataPath, SheetName);
	
	if(vatAmountindex != (AllItemIds.length - 1 )) {
		
		Utilities_I19All.utilityFileWriteOP.appendValueToCell(",", r, 46, TestDataPath, SheetName);

	}
	
	
}
						 
							 Boolean RestPost = McColls_I19_PostCall.PostCallOrderCreationGeneralSeperateItemDetails(TestDataPath, ItemDetailsSheetName, ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0],OrderCount, ShipToDeliverAt, AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes, AllItemAttributes, TestKeyword, TestCaseNo,ResultPath,xwpfRun);
							 
							 if(RestPost==true) {
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
								}
										
							 else{
								 
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
								 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							 	}
							 
							 post = post && RestPost;
							 
		
							 
							 
							 
							 
							 
							 
  RestGet = McColls_I5a_GetCall.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun);

			
							 
							 
							 
							 
		if(RestGet==true) {
		Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Order Validated Successfully.", "PASS", ResultPath);
								
							 
				 }
										
		else{
			 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Service Call", "Error Occured during Order Validation.", "FAIL", ResultPath);
								 
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				}
							 
							
							 
							
							 RestGet=get && RestGet;			 
							 
							 
							 
							 
							 
							 
							 
							 
							 
RestGetItem = McColls_I5a_GetCall.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "confirmed", TestCaseNo,ResultPath,xwpfRun);
							 
							 if(RestGetItem==true) {
								 
								 
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
								}
										
							 else{
								 
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
								 
								 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							 
							 }
							 
							 
							 get = get && RestGetItem && RestGet;
							
							 
							 Thread.sleep(5000);
			 
		
					 
							 
//RestShip = ShipCall_I19.ShipCallOrderShip(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, QuantitytoShip, TestCaseNo,ResultPath,xwpfRun);
							 	 
	RestShip = ShipCall_I19.ShipCallOrderShipment_New(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, QuantitytoShip, TestCaseNo,ResultPath,xwpfRun);
			
if(RestShip==true) {
	
	 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Ship Order Service Call", "Order Shipped Successfully.", "PASS", ResultPath);
	}
		
else{
	
	Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Ship Order Service Call", "Error Occured during Order Shipped.", "FAIL", ResultPath);
	
	throw new MyException("Test Stopped Because of Failure. Please check Execution log");
}							 
					 
						 
							 ship = ship && RestShip;	
							 
							 
	RestShipppedClosure = ShipCall_I19.ShipCallOrderShipment_NewClosure(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], MessageType[0], ShipToLocationId[0], ShipToDeliverAt, ItemIDs, QuantitytoShip, TestCaseNo,ResultPath,xwpfRun);
								
							 if(RestShipppedClosure==true) {
							 	
							 	 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Shipment Closure Order Service Call", "Order closure Shipped Successfully.", "PASS", ResultPath);
							 	}
							 		
							 else{
							 	
							 	Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "4", "Shipment Closure Order Service Call", "Error Occured during Order Shipped.", "FAIL", ResultPath);
							 	
							 	throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							 }							 
							 					 
							 						 
							 							 ship = ship && RestShip && RestShipppedClosure;	
	
							 
							 
							 
							 		   
RestGetAfterShip = McColls_I19_GetCall.GetCallItemStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID[order], "shipped", TestCaseNo,ResultPath,xwpfRun);
							 
if(RestGetAfterShip==true) {
	 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "5", "Get Order Service Call After Order Shipped", "Shipped Order Validated Successfully.", "PASS", ResultPath);
	}
			
else{
	 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "5", "Get Order Service Call After Order Shipped", "Error Occured during Shipped Order Validation.", "FAIL", ResultPath);

throw new MyException("Test Stopped Because of Failure. Please check Execution log");
}
	 						 
							 aftership = aftership && RestGetAfterShip;	
							 
	/*					 
	String TotalAmount = McColls_I19_Utilities.checkTotalAmountInPending(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, NasPath, TemporaryFilePath, OrderID[order], TestCaseNo, ResultPath);		 
							 
						System.out.println("The total amount is "+TotalAmount);	
						
						double TotalAmountinDouble = Double.parseDouble(TotalAmount);
						
						DecimalFormat df = new DecimalFormat("0.00");
						
							 
						 excelCellValueWrite.writeValueToCell(df.format(TotalAmountinDouble), r, 50, TestDataPath, SheetName);
	 
							 

					if( (post) && (get)&& (ship)&&(aftership)) */
				 
				 if( (post) && (get)&& (ship) && (aftership)){
					 
					 Final_Result = "PASS";
					 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					 
					 Assert.assertTrue(TestCaseName, true);
				 }
				 else {
					 Final_Result = "FAIL";
					 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
					 
					 Assert.assertTrue(TestCaseName, false);
					 
				 }
				 
				 
			}
			
		}
		
		}
		catch(Exception e) {
			e.printStackTrace();

			 Final_Result = "FAIL";
			 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
			 
			 Assert.assertTrue(TestCaseName, false);
		}
		finally
		 {     FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"1.docx");
			doc.write(out1);
		       out1.close();

			
			 //Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
		        RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		 }
			
		}
}

