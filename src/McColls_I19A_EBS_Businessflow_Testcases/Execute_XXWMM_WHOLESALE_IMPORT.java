package McColls_I19A_EBS_Businessflow_Testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import McColls_I5A_EndToEnd_Testcases.BatchJoB_GetWholesaleOrderService;
import Utilities_i5A_All.*;

public class Execute_XXWMM_WHOLESALE_IMPORT {

	@Test
	public void test() throws BiffException, IOException {
		
		

		String JobName=ProjectConfigurations.LoadProperties("McColls_I19a_Appworx");
		String TCName=ProjectConfigurations.LoadProperties("McColls_I19a_AppworxTC");
		String AppworxURL=ProjectConfigurations.LoadProperties("McColls_I19a_AppworxURLSIT");
		String AppworxUserName=ProjectConfigurations.LoadProperties("McColls_I19a_AppworxUserName");
		String AppworxPassword=ProjectConfigurations.LoadProperties("McColls_I19a_AppworxPassword");
		
		
		System.out.println("UserName"+AppworxUserName);
		
		
		System.out.println("Password"+AppworxPassword);
		
				
//		JobName="GET_WHOLESALE_ORDERS_SERVICE";
		JobName="WMM_WHOLESALE_IMPORT_SCHEDULER";
		
		//String TestCaseNo="TC_01";

		//String DataFilePath="S:\\MTEBS\\Automation\\McColls_I23_Selenium\\McCollsI23\\TestData\\Appworx_Login_Local.xls";

		String ResultPath="S:\\MTEBS\\Automation\\McColls_I23_Selenium\\McCollsI23\\Results";
		
		
		//Boolean Res=true;

//	Boolean Res=BatchJoB_XXWMM_WHOLESALE_IMPORT.RunJob(JobName, TCName,ResultPath,AppworxUserName,AppworxPassword,AppworxURL);
		
		Boolean Res=BatchJoB_XXWMM_WHOLESALE_IMPORT.RunJob(AppworxURL,AppworxUserName,AppworxPassword,TCName,ResultPath,JobName);

	if(Res){
			
				System.out.println(JobName+" job executed successfully ");				
				utilityFileWriteOP.writeToLog("Batch Job Execution", JobName, "PASS");

		}

			
	else{
					
				System.out.println("Execution failed  for the batch job   "+JobName);
				utilityFileWriteOP.writeToLog("Batch Job Execution", JobName, "FAIL");

		  }	
		}

	}
