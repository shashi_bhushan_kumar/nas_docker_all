package McColls_EBS_I19AB_Functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import Utilities_I19ab.utilityFileWriteOP;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class WinSCPCall {
	
	
	
public static String InvoiceExtract_TotalAmount(String OrderID, String tcid,String TempDownloadedOrderFilePath) throws IOException {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		String InvoiceID = null;
		
		String Result = null;
		int flag = 0;
		
		try {
			
			session = jsch.getSession("mwadmin", "tgmus03x", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("2Bchanged");
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
		/*	sftpchannel.cd("cd..;");
			sftpchannel.cd("cd..;");*/
			
			
			sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
				
				//Read from local
				br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
				
				int count = 0;
				String CurrentLine = null;
				
				while((CurrentLine = br.readLine()) != null) {
					
					//Split the Current line in comma seperated values
					String[] strarr = CurrentLine.split(",");
				
					if(strarr[1].equals(OrderID)) {
						
						InvoiceID = strarr[11];
						System.out.println("The Invoice id is " + InvoiceID);
						utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderID+"is ", InvoiceID);
						
						Result = InvoiceID;
						flag = 1;
						break;
					}
					
					
					
					
					
					
					
				
					
				}
				
				if(flag == 1) {
					
					br.close();
					break;
					
				}
				
				
		
				
			}
			
			br.close();
			sftpchannel.exit();
			
			session.disconnect();
			
		
			
			
		
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
			return Result;
		}
		
		
		
	
		
		return Result;
	}
	
	
	
	
	
	
	
	
public static String InvoiceExtract(String OrderID, String tcid,String TempDownloadedOrderFilePath) throws IOException {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		String InvoiceID = null;
		
		String Result = null;
		int flag = 0;
		
		try {
			
			session = jsch.getSession("mwadmin", "tgmus03x", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("2Bchanged");
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
		/*	sftpchannel.cd("cd..;");
			sftpchannel.cd("cd..;");*/
			
			
			sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
				
				//Read from local
				br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
				
				int count = 0;
				String CurrentLine = null;
				
				while((CurrentLine = br.readLine()) != null) {
					
					//Split the Current line in comma seperated values
					String[] strarr = CurrentLine.split(",");
				
					if(strarr[1].equals(OrderID)) {
						
						InvoiceID = strarr[11];
						System.out.println("The Invoice id is " + InvoiceID);
						utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderID+"is ", InvoiceID);
						
						Result = InvoiceID;
						flag = 1;
						break;
					}
					
				
					
				}
				
				if(flag == 1) {
					
					br.close();
					break;
					
				}
				
				
		
				
			}
			
			br.close();
			sftpchannel.exit();
			
			session.disconnect();
			
		
			
			
		
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
			return Result;
		}
		
		
		
	
		
		return Result;
	}
	
	
public static ArrayList<HashMap<String, String>> ValidateNASFile(String OrderID, String tcid,String TempDownloadedOrderFilePath,String Items) throws IOException {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		String InvoiceID = null;
		
		String Result = null;
		
		String Nas_Item=null;
		
		int flag = 0;
		
		String []   ItemList=Items.split(",");
		
		int itemCount=ItemList.length;
	
		 String ClientName=null;
		
		 String OrderType=null;
		
		ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> hm = null;
		
		//hm = new HashMap<String, String>();
		
		
		try {
			
			session = jsch.getSession("mwadmin", "tgmus03x", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("2Bchanged");
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
		/*	sftpchannel.cd("cd..;");
			sftpchannel.cd("cd..;");*/
			
			
			sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
				
				//Read from local
				br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
				
				int count = 0;
				String CurrentLine = null;
				
				int cntTrack=0;
				
	while((CurrentLine = br.readLine()) != null) {
					
					//Split the Current line in comma seperated values
					String[] strarr = CurrentLine.split(",");
				
					if(strarr[1].equals(OrderID)) {
						
						hm = new HashMap<String, String>();
						
						InvoiceID = strarr[11];
						
						ClientName=strarr[0];
						
						OrderType=strarr[5];
						
						Nas_Item=strarr[2];
						
						
						hm.put("InvoiceID", InvoiceID);
						hm.put("ClientName", ClientName);
						hm.put("OrderType", OrderType);
						hm.put("ClientName", ClientName);
						hm.put("Nas_Item", Nas_Item);
						hm.put("OrderID", strarr[1]);
						
						
						ListofMaps.add(hm);
						
						
						cntTrack=cntTrack+1;

						
						System.out.println("The Invoice id is " + InvoiceID);
						utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderID+"is ", InvoiceID);
						
						Result = InvoiceID;
						
						
						
						if(cntTrack==itemCount){
						
							flag = 1;
							break;
							
						}
						
					}
					
				
					
				}
				
				if(flag == 1) {
					
					br.close();
					break;
					
				}
				
				
		
				
			}
			
			br.close();
			sftpchannel.exit();
			
			session.disconnect();
			
		
			
			
		
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
			
		}
		
		
		
	
		
		return ListofMaps;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean RaiseOrder() {
		
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader br = null;
		
		String InvoiceID = null;
		
		String RaiseOrderPath = "/data/nas/sit/wholesale_amazon/events/raise/pending/";
		
		boolean res = false;
		int flag = 0;
		
		try {
			
			session = jsch.getSession("mwadmin", "tgmus03x", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword("2Bchanged");
			session.connect();
			
			Channel channel = session.openChannel("sftp");
			channel.connect();
			
			ChannelSftp sftpchannel = (ChannelSftp) channel;
			
			sftpchannel.cd(RaiseOrderPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			//Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
			
			sftpchannel.put(new FileInputStream(new File("Test Data/RaiseOrderExcel.csv")), RaiseOrderPath+"RaiseOrderExcel.csv");
			
			res = true;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res = false;
			return res;
		}
		
		return res;
		
	}

}
