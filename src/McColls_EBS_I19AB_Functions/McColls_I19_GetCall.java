package McColls_EBS_I19AB_Functions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;







import org.apache.poi.xwpf.usermodel.XWPFRun;

import Utilities_I19ab.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class McColls_I19_GetCall {
	
	public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	       	
	        	Thread.sleep(5000);
	        	
	        	
	        	for (int i=0;i<10;i++){
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        
	        
	        int ItemCount=jarray.size();
	        
	        int resCount=0;
	        
	        for(int k=0; k<jarray.size(); k++) {
	        	
	        	jsonobject = jarray.get(k).getAsJsonObject();
		       
	        	
	        	
	        	String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 7) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	//break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
					
					resCount=resCount+1;
					
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	//break;
		        }
	        	
	        }
	        	  
	        
	        if(ItemCount==resCount) {
	        	
	        	res=true;
	        	break;
	        }
	        
	      }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
		}
			
		
	    
	    finally{
	        
	        response.close();
	        return res;
	        
	}
	    
	 
	    
		
	}
	
public static boolean GetCallItemStatusValidationOnlyConfirmed(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(waitingTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne != 7) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is not 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	break;
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
public static String GetCallItemDetailsFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String itemBaseType, String quantityType, String itemLineId, String quantityOrdered, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	int MaxwaitingTime = 60000;
	
	int regularWaitingTime = 10000;
	
	HashMap<String, String> mapofMaps = new HashMap<String, String>();
	HashMap<String, String> mapofPrices = new HashMap<String, String>();

	String priceOrderedCurrency = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
        	
        	String description = jsonobject.get("description").toString().replaceAll("^\"|\"$", "");
        	
        	//String mapsJsonBody = jsonobject.get("maps").toString().replaceAll("^\"|\"$", "");
	        JsonArray jarrayMaps = jsonobject.getAsJsonArray("maps");
	        
	        for(int mapsindex=0; mapsindex<jarrayMaps.size(); mapsindex++) {
	        	
	        	JsonObject jsonobjectMapsBody = jarrayMaps.get(mapsindex).getAsJsonObject();
	        	
	        	String mapJsonKey = jsonobjectMapsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayMapsBody = jsonobjectMapsBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectMapsBodyValue = jarrayMapsBody.get(0).getAsJsonObject();
		        
		        String mapJsonvalue = jsonobjectMapsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofMaps.put(mapJsonKey, mapJsonvalue);

	        	
	        }
	        
		    JsonArray jarrayPrices = jsonobject.getAsJsonArray("prices");

	        
	        for(int pricesindex=0; pricesindex<jarrayPrices.size(); pricesindex++) {
	        	
	        	JsonObject jsonobjectPricesBody = jarrayPrices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectPricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
		        JsonArray jarrayPricesBody = jsonobjectPricesBody.getAsJsonArray("values");
		        
		        JsonObject jsonobjectPricesBodyValue = jarrayPricesBody.get(0).getAsJsonObject();
		        
		        String pricesJsonvalue = jsonobjectPricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
		        
		        mapofPrices.put(pricesJsonKey, pricesJsonvalue);
		        
		        if(pricesJsonKey.equals("WSP")) {
		        	
			        priceOrderedCurrency = jsonobjectPricesBodyValue.get("currency").toString().replaceAll("^\"|\"$", "");
			        mapofPrices.put("priceOrderedCurrency", priceOrderedCurrency);

		        	
		        }

	        	
	        }
	        
	       	//fetching  priceOrderedCurrency
	        
	        
		    
		    System.out.println(mapofMaps.size());
		    System.out.println(mapofPrices.size());
		    
		    String itemDetailsbody = "{\"quantityType\": \"" + quantityType + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    System.out.println(itemDetailsbody);	    
		    	     
		    res = itemDetailsbody;
			
			
        	
        }
        	  
       
        
       // }
    	
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}


public static String GetCallquantityTypeFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "Item ID "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
     /*   for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularWaitingTime);*/
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	
	        
		    JsonArray jarraylookups = jsonobject.getAsJsonArray("lookups");
		    
		    HashMap<String, Long> mapofquantityType = new HashMap<String, Long>();
		    ArrayList<String> listoflookupkeys = new ArrayList<String>();
		    ArrayList<Long> listoflookupstartvalues = new ArrayList<Long>();

		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

		    
		    for(int lookupsindex=0; lookupsindex<jarraylookups.size(); lookupsindex++) {
		    	
		    	JsonObject jsonobjectlookupsBody = jarraylookups.get(lookupsindex).getAsJsonObject();
	        	
	        	String lookupsJsonKey = jsonobjectlookupsBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraylookupsBody = jsonobjectlookupsBody.getAsJsonArray("values");
		        
	        	if(lookupsJsonKey.equals("SINGLE-PICK")) {
	        		
	        		
	        		for(int lookupsBodyindex=0; lookupsBodyindex<jarraylookupsBody.size(); lookupsBodyindex++) {
		        		
				        JsonObject jsonobjectlookupsBodyValue = jarraylookupsBody.get(lookupsBodyindex).getAsJsonObject();

				        String lookupsJsonvalue = jsonobjectlookupsBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
				        String lookupsJsonstartdate = jsonobjectlookupsBodyValue.get("start").toString().replaceAll("^\"|\"$", "");
				       System.out.println(lookupsJsonvalue + " " + lookupsJsonstartdate);
				        Date date = formatter.parse(lookupsJsonstartdate.replaceAll("Z$", "+0000"));
				    	
				    	long startdatemilli = date.getTime();
				        
				        mapofquantityType.put(lookupsJsonvalue, startdatemilli);
		        		
		        	}
	        	
 	        		
	        	}
	    	
		    }
		    
		    for(String str: mapofquantityType.keySet()) {
		    	
		    	listoflookupkeys.add(str);
		    	listoflookupstartvalues.add(mapofquantityType.get(str));
		    }

		    long latestdatemilli = 0; 
		    
		    for(int startdateindex=0; startdateindex<listoflookupstartvalues.size(); startdateindex++) {
		    			    	
		    	if(listoflookupstartvalues.get(startdateindex) > latestdatemilli) {
		    		
		    		latestdatemilli = listoflookupstartvalues.get(startdateindex);
		    		
		    	}
		    	
		    }
		    
		    String finalSinglePickkey = null;
		    String quantityTypeFromWebPortal = null;
		    
		    for(int mapofquantityTypeindex=0; mapofquantityTypeindex<mapofquantityType.size(); mapofquantityTypeindex++) {
		    	
		    	if( (mapofquantityType.get(listoflookupkeys.get(mapofquantityTypeindex))) == latestdatemilli) {
		    		
		    		finalSinglePickkey = listoflookupkeys.get(mapofquantityTypeindex);
		    	}
		    	
		    }
		    
		    System.out.println("latest date in milli "+latestdatemilli);
		    
		    if(finalSinglePickkey.equals("Y")) {
		    	
		    	quantityTypeFromWebPortal = "EA";
		    }
		    
		    if(finalSinglePickkey.equals("N")) {
		    	
		    	quantityTypeFromWebPortal = "CS";
		    }
		    
		    
		    //String itemDetailsbody = "{\"quantityType\": \"" + quantityTypeFromWebPortal + "\",\"itemId\": \"" + ItemId +"\",\"itemAlternateId\": {\"clientId\": \"" + mapofMaps.get("CLIENTID") +"\",\"barcodeEan\": \"" + mapofMaps.get("EAN") +"\",\"skuPin\": \"" + mapofMaps.get("PIN") +"\"},\"priceOrderedAmount\": " + mapofPrices.get("WSP") + ",\"priceOrderedCurrency\": \"" + priceOrderedCurrency + "\",\"itemBaseType\": \"" + itemBaseType + "\",\"itemLineId\": \"" + itemLineId + "\",\"quantityOrdered\": " + quantityOrdered + ",\"priceOrderedTaxRate\": " + mapofPrices.get("VAT") + ",\"itemDescription\": \"" + description + "\",\"itemCaseSize\": " + mapofPrices.get("CS").substring(0, mapofPrices.get("CS").indexOf(".")) + "}";
		    	    
		    //System.out.println(itemDetailsbody);	    
		    	     
		    res = quantityTypeFromWebPortal;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}

public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	        if(countOne == 10) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 10");
	        }
	        else {
	        	
	        	res = false;
				
	        	
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static boolean GetCallItemStatusValidation_negativeScenarios(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid,String ResultPath,XWPFRun xwpfrun) throws IOException {
	
	boolean res = false;
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",ResultPath,xwpfrun,"");
        
        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
        	
        	Thread.sleep(regularwaitTime);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),ResultPath,xwpfrun,"");
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,ResultPath,xwpfrun,"");
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        
        int stCnt=jarray.size();
        
        int mtchCnt=0;
        
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        String statusValidationResult = jsonobject.get("statusValidation").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("StatusValidation Result is " + statusValidationResult);
	        
	        int countOne = statusValidationResult.split("1").length - 1;
	        
	      /*  if(countOne == 9) {
	        	res = true;
	        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
				System.out.println("Count of 1 is 9");
	        }
	        else {
	        	
	        	res = false;
				
	        	
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

	        	//break;
	        }*/
	        System.out.println(countOne);
	        System.out.println(statusCurrent);
	        System.out.println(statusCurrentValue);
	        
	        if( (statusCurrent.equals(statusCurrentValue)) && (countOne != 9)) {
	        	res = true;
	        	
				utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
				
				mtchCnt=mtchCnt+1;
				
				
			//break;
	        }
	        
	        else{
	        	res = false;
	        	utilityFileWriteOP.writeToLog(tcid, "StatusValidation is "+statusValidationResult.replaceAll("^\"|\"$", ""), "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");

				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""),ResultPath,xwpfrun,"");
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }
        	
        }
        	  
       
        if(mtchCnt==stCnt) {
        	break;
        
        }
        
      }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
}
    
  
    
	
}

public static String GetCallVatAmountFromWebPortal(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String ItemId, String tcid, String Resultpath) throws IOException {
	
	String res = null;
	
	String EBSVatDetails = null;
	
	String pricesJsonvalue = null;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        //https://sit-api.morrisons.com/wholesaleorder/v1/customers/mccolls/catalogues/main/items/101743031?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36&filtertype=maptype&filtervalue=PIN
        
        HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/mccolls/catalogues/main/items/" + ItemId + "?" + ApiKey+"&filtertype=maptype&filtervalue=PIN");
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Item ID is " + ItemId);
        utilityFileWriteOP.writeToLog(tcid, "Searching the attributes for ", "ItemId "+ItemId,Resultpath);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done",Resultpath);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode(),Resultpath);
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse,Resultpath);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(),Resultpath);
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	        
        	JsonArray jarrayprices = jsonobject.getAsJsonArray("prices");
		    
		    for(int pricesindex=0; pricesindex<jarrayprices.size(); pricesindex++) {
		    	
		    	JsonObject jsonobjectpricesBody = jarrayprices.get(pricesindex).getAsJsonObject();
	        	
	        	String pricesJsonKey = jsonobjectpricesBody.get("type").toString().replaceAll("^\"|\"$", "");
	        	
	        	JsonArray jarraypricesBody = jsonobjectpricesBody.getAsJsonArray("values");
		        
        		
	        		if(pricesJsonKey.equals("VAT")) {
	        			
	        			
	        			for(int pricesBodyindex=0; pricesBodyindex<jarraypricesBody.size(); pricesBodyindex++) {
			        		
					        JsonObject jsonobjectpricesBodyValue = jarraypricesBody.get(pricesBodyindex).getAsJsonObject();

					        pricesJsonvalue = jsonobjectpricesBodyValue.get("value").toString().replaceAll("^\"|\"$", "");
					     
			        	}	        		
	        	}	        	
		    }
		   
		    Double vatRatevalueDouble = Double.parseDouble(pricesJsonvalue);
		    
		    
		    int vatRateInt = vatRatevalueDouble.intValue();
		    
		    if( (vatRateInt <= 5) && (vatRateInt > 0) ) {

		    	EBSVatDetails = "LOWER";
		    }
		    else

		    	if(vatRateInt >= 20) {

		    		EBSVatDetails = "STANDARD 20";
		    	}
		    	else
		    		if(vatRateInt == 0) {
		    			EBSVatDetails = "ZERO";
		    		}
		    		else
		    		{
		    			EBSVatDetails = "MIXED";
		    		}
		    
		    
		   	     
		    res = EBSVatDetails;
			
			
        	
        }
        	  
       
	} catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e,Resultpath);
        
		res = null;
		return res;
	}
    
    finally{
        
        response.close();
        
}
    
    return res;
    
	
}



}
