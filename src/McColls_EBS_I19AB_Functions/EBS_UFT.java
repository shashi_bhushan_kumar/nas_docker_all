package McColls_EBS_I19AB_Functions;

import java.io.IOException;

public class EBS_UFT {
	
	
	
	public static boolean EBS_InvoiceValidation_Rontec(int rowNum){
		
		 Boolean res=false;
		
			 try {

			       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );

			      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_SCRIPTS_AmazonWholesale/EBS_InvoiceValidation_Rontec.vbs"+" "+rowNum);

			      System.out.println("Waiting for UFT Script Execution to be completed ...");
			        try {
						p.waitFor();
						
						
						res=true;
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			 
			 
			 catch (IOException ex) {

			    }
		
		
		return res;
		
		
		
		}
	
	
	
	
	
	public static boolean EBS_InvoiceValidation_SandPiper(int rowNum){
		
		 Boolean res=false;
		
			 try {

			       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
				 
				// "Arg with spaces"
				 
				 
				
			      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_SCRIPTS_AmazonWholesale/EBS_InvoiceValidation_SandPiper.vbs"+" "+rowNum);

			      System.out.println("Waiting for UFT Script Execution to be completed ...");
			        try {
						p.waitFor();
						
						
						res=true;
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			 
			 
			 catch (IOException ex) {

			    }
		
		
		return res;
		
		
		
		}
	

	
	public static boolean EBS_InvoiceValidation_Standard_amazon(int rowNum){
	
	 Boolean res=false;
	
		 try {

		       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
			 
			// "Arg with spaces"
			
		      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_Scripts_Exec_Source/Shipment_EBS_Validate/EBS_InvoiceValidation_Standard_Order.vbs"+" "+rowNum);

		      System.out.println("Waiting for UFT Script Execution to be completed ...");
		        try {
					p.waitFor();
					
					
					res=true;
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		 
		 
		 catch (IOException ex) {

		    }
	
	
	return res;
	
	
	
	}
	
	public static boolean EBS_InvoiceValidation(){
		
		 Boolean res=false;
		
			 try {

			       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
				 
				// "Arg with spaces"
				
			      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_SCRIPTS_AmazonWholesale/EBS_InvoiceValidation.vbs" );

			      System.out.println("Waiting for UFT Script Execution to be completed ...");
			        try {
						p.waitFor();
						
						
						res=true;
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			 
			 
			 catch (IOException ex) {

			    }
		
		
		return res;
		
		
		
		}
		
	
	
	public static boolean EBS_InvoiceValidation_Storepick(int rowNum){
		
		 Boolean res=false;
		
			 try {

			       // Runtime.getRuntime().exec( "C:/UFTJOB/MyTest.vbs" );
				 
				// "Arg with spaces"
				
			      Process p=  Runtime.getRuntime().exec( "cscript C:/UFT_SCRIPTS_AmazonWholesale/EBS_InvoiceValidation_Storepick.vbs"+" "+rowNum);

			      System.out.println("Waiting for UFT Script Execution to be completed ...");
			        try {
						p.waitFor();
						
						
						res=true;
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			 
			 
			 catch (IOException ex) {

			    }
		
		
		return res;
		
		
		
		}
	

}
