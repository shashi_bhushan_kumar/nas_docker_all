package Functions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;





import Utilities_I19ab.utilityFileWriteOP;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;



public class WinSCPCall {

	//******To get the latest file extract generated after batch within 20 mins(latest file )
	
	
		public static String latestFileExtract_SFTP(String OrderId,String us, String pw,String oretailpath,String host, String port, String tcid,String ResultPath,XWPFRun xwpfRun) throws IOException {

			String res="";
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			String Result = null;
			int flag = 0;

			long lastmodifiedfiletime = 0;
			String lastmodifiedfilename = null;

			try {

				session = jsch.getSession(us, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				System.out.println("Connection successful");
				Channel channel = session.openChannel("sftp");
				channel.connect();
				System.out.println("Channel Connection successful");
				ChannelSftp sftpchannel = (ChannelSftp) channel;


				sftpchannel.cd(oretailpath);

				System.out.println("The Current path is " + sftpchannel.pwd());

				//w-mcc-OrderID.csv

				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-"+OrderId+"*.csv");
				
				String filename="";
					
				
				int p=0;
				
				for(ChannelSftp.LsEntry entry: list) {

					 filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					
					//Getting the current date
					Date time=new Date();
					 //This method returns the time in millis
					long timeMilli = time.getTime();
					
					System.out.println("The current time in miiliseconds:"+timeMilli);
					
					Calendar calendar = Calendar.getInstance();
					System.out.println("Current Date = " + calendar.getTime());
					System.out.println("Time in milliseconds before= "+(long)calendar.getTimeInMillis());
					
					
				  // Subtract 15 minutes from current date
					Calendar calendar1 = Calendar.getInstance();
				      calendar1.add(Calendar.MINUTE, -100005);
				      System.out.println("Updated Date = " + calendar1.getTime());
				      System.out.println("Time in milliseconds after = "+(long)calendar1.getTimeInMillis());

					
		long currentfilelastmodifiedmilliseconds =(long) entry.getAttrs().getMTime()*1000L;
					
					
					System.out.println("The current file time is:"+currentfilelastmodifiedmilliseconds);

					
					
		if(currentfilelastmodifiedmilliseconds < calendar.getTimeInMillis() && calendar1.getTimeInMillis()<currentfilelastmodifiedmilliseconds){
						
						
						lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;
						lastmodifiedfilename = filename;
						res=filename;
					//	utilityFileWriteOP.writeToLog(tcid, "The target file name in "+oretailpath, " is "+lastmodifiedfilename, ResultPath, xwpfRun,"");
						
						System.out.println("The file is :"+filename);
						
						break;
					}
					else{
						
						System.out.println("There are no file generated recently");
						
						res=null;
					}
				}
						
				//br.close();
				sftpchannel.exit();

				session.disconnect();

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println(e);
			//	utilityFileWriteOP.writeToLog(tcid, "Error occurred during File Extraction ", "Due to :"+e);
				return res;
			}


			return res;
		}
		
		
		//*********To Fetch the Invoice ID from the Shipment file
		
		
		public static String fetchInvoiceId(String OrderId,String us, String pw,String host, String port, String tcid,String TempDownloadedOrderFilePath,String ResultPath,XWPFRun xwpfRun) throws IOException {
			
			JSch jsch = new JSch();
			Session session = null;
			BufferedReader br = null;
			
			String InvoiceID = null;
			
			String Result = null;
			int flag = 0;
			
			try {
				
				session = jsch.getSession(us,host , Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pw);
				session.connect();
				
				Channel channel = session.openChannel("sftp");
				channel.connect();
				
				ChannelSftp sftpchannel = (ChannelSftp) channel;
				
			/*	sftpchannel.cd("cd..;");
				sftpchannel.cd("cd..;");*/
				
				
				sftpchannel.cd("/data/nas/sit/wholesale_amazon/wholesale_ebs/out/pending/");
				
				System.out.println("The Current path is " + sftpchannel.pwd());
				
				Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("w-mcc-*.csv");
				
				for(ChannelSftp.LsEntry entry: list) {
					
					String filename = entry.getFilename();
					
					System.out.println("The current file is " + filename);
					sftpchannel.get(filename, TempDownloadedOrderFilePath+"/" + filename);//Copy file from WinSCP to local
					
					//Read from local
					br = new BufferedReader(new FileReader(TempDownloadedOrderFilePath+"/" + filename));
					
					int count = 0;
					String CurrentLine = null;
					
					while((CurrentLine = br.readLine()) != null) {
						
						//Split the Current line in comma seperated values
						String[] strarr = CurrentLine.split(",");
					
						if(strarr[1].equals(OrderId)) {
							
							InvoiceID = strarr[11];
							System.out.println("The Invoice id is " + InvoiceID);
							utilityFileWriteOP.writeToLog(tcid, "The Invoice for OrderID "+OrderId+"is ", InvoiceID);
							
							Result = InvoiceID;
							flag = 1;
							break;
						}
					}
					
					if(flag == 1) {
						
						br.close();
						break;
					}
				}
				
				br.close();
				sftpchannel.exit();
				
				session.disconnect();
				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
				utilityFileWriteOP.writeToLog(tcid, "Error occurred during Invoice Extraction ", "Due to :"+e);
				return Result;
			}
			
			
			return Result;
		}

			
	
}
